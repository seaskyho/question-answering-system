import csv, json, re, nltk, spacy, operator, time
from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize
from collections import defaultdict
from math import fabs
from nltk.tokenize import RegexpTokenizer

nlp = spacy.load('en_core_web_md')
nlpSimi = spacy.load('en_vectors_web_lg')

stopwords = set(nltk.corpus.stopwords.words('english'))
lemmatizer = nltk.stem.wordnet.WordNetLemmatizer()
Regtokenizer = RegexpTokenizer(r'\w+')  # remove punctuation
stemmer = nltk.stem.PorterStemmer()

"""  **********************************
import "documents.json" file 
*********************************** """

with open("documents.json", 'r') as jsDoc:
    documents = json.load(jsDoc)
jsDoc.close()

""" **********************************
import "training.json" file 
********************************** """

with open("training.json", 'r') as jsTrain:
    trainings = json.load(jsTrain)
jsTrain.close()

""" **********************************
import "testing.json" file 
********************************** """

with open("testing.json", 'r') as jsTest:
    testings = json.load(jsTest)
jsTest.close()

""" **********************************
import "devel.json" file 
********************************** """

with open("devel.json", 'r') as jsDev:
    devels = json.load(jsDev)
jsDev.close()

""" **********************************
Helper Functions
********************************** """


def quesKeyWords(question, queryExpansion=False):
    """ 
    extract keywords in question
    :type: question - string
    :rtype: list 
    """
    words = word_tokenize(question)
    keywords = [lemmatizer.lemmatize(w) for w in words if lemmatizer.lemmatize(w) not in stopwords]

    return keywords


def buildSents(document, ques_keywords, questionRAW):
    """
    build list of sentences (in Spacy obj) from document
    :param document: 
    :return: string
    """
    sent_score = {}

    ques_kw_stemmed = [stem(word) for word in ques_keywords if word.isalnum()]  # remove punctuation

    for para in document:
        sents = sent_tokenize(para)
        for sent in sents:
            words = word_tokenize(sent.lower())
            sent_keywords = [stem(lemmatizer.lemmatize(w)) for w in words if
                             w.isalnum() and lemmatizer.lemmatize(w) not in stopwords]  # remove punctuation
            # num_of_keywords
            ques_keyW_set = set(ques_kw_stemmed)
            num_of_keywords = 0

            appeared_keyword = []
            keyword_index = []
            gap = 0
            for index in range(len(sent_keywords)):
                if sent_keywords[index] in ques_keyW_set and sent_keywords[index] not in appeared_keyword:
                    appeared_keyword.append(sent_keywords[index])
                    num_of_keywords += 1
                    keyword_index.append(index)
            if len(keyword_index) > 1:
                gap = keyword_index[-1] - keyword_index[0]

            sent_score[sent] = num_of_keywords - 0.03 * gap
    return max(sent_score.items(), key=operator.itemgetter(1))[0]


ansWeights = {
    # handleTokenLevel
    "token_exist": 1,
    "token_DisMul": -0.01,  # multiplier for token distance
    "token_posNOUN": 0.0,  # if token pos is NOUN, give a bonus (or back to 0.1??)

    # handleWhen
    "when_matchNE": 0.11,  # match Named Entity
    "when_matchTAG": 0.2,  # tag is a match
    # handleWhere
    "where_matchNE": 0.5,
    "where_Chunk": 0.2,
    "where_ChunkMatchRootDep": 0.2,
    "where_TokenMatchDep": 0.1,
    "where_ChunkDisMul": -0.005,
    # handleQuantity
    "quantity_matchNE": 0.5,
    "quantity_matchTAG": 0.2,
    # handle Person
    "person_matchNE": 0.22,
    "person_Chunk": 0.2,
    "person_ChunkDisMul": -0.005,  # multiplier for chunk distance

    # handle which
    "which_Chunk": 0.2,
    "which_ChunkDisMul": -0.01,  # multiplier for chunk distance
    "which_Simi": 0.5,  # multiplier for similarity

    # handle what
    "what_Chunk": 0.2,
    "what_ChunkDisMul": -0.01,  # multiplier for chunk distance
    "what_Simi": 0.5,  # multiplier for similarity

    # handle why
    # handle default
}


def handleWhenS(questionNLP, ques_keywords, sentNLP, sentRaw):
    """

    :param questionNLP: nlp(question)
    :param ques_keywords: list of strings
    :param sentNLP: nlp(sent)
    :param sentRaw: raw sentence string
    :return: predicted best answer
    """
    ansDict = defaultdict(float)  # potential answers and their score

    for ent in sentNLP.ents:
        if ent.label_ in ['DATE', 'TIME']:
            ansDict[ent.text] += ansWeights['when_matchNE']
            if ent.lemma_ not in ques_keywords:
                ansDict[ent.text] += 0.2  # a bonus

    for token in sentNLP:
        if token.like_num and token.lemma_ not in ques_keywords and token.lemma_ not in stopwords and not token.is_punct:
            ansDict[token.text] += ansWeights['when_matchTAG']

    if len(ansDict) == 0:
        childTokens = []  # token new in sent
        masterTokens = []  # token appear in both sent and question
        for token in sentNLP:
            if token.lemma_ not in stopwords and not token.is_punct:
                if token.lemma_ in ques_keywords:
                    masterTokens.append(token)
                else:
                    childTokens.append(token)

        for child in childTokens:
            ansDict[child.text] += ansWeights["token_exist"]
            if len(masterTokens) > 0:
                for master in masterTokens:
                    ansDict[child.text] += fabs(master.idx - child.idx) * ansWeights["token_DisMul"]
            if child.pos_ == 'NOUN':
                ansDict[child.text] += ansWeights["token_posNOUN"]

    cutoff = 2
    if len(ansDict) == 0:
        bestANS = ""
    elif len(ansDict) <= cutoff:
        bestANS = " ".join(key for key in ansDict.keys())
    else:
        sorted_keys = sorted(ansDict, key=ansDict.get, reverse=True)[:cutoff]
        bestANS = " ".join(sorted_keys)
    return bestANS


def handleWhen(questionNLP, ques_keywords, sentNLP, sentRaw):
    """

    :param questionNLP: nlp(question)
    :param ques_keywords: list of strings
    :param sentNLP: nlp(sent)
    :param sentRaw: raw sentence string
    :return: predicted best answer
    """
    ansDict = defaultdict(float)  # potential answers and their score

    for ent in sentNLP.ents:
        if ent.label_ in ['DATE', 'TIME']:
            ansDict[ent.text] += ansWeights['when_matchNE']
            if ent.lemma_ not in ques_keywords:
                ansDict[ent.text] += 0.2  # a bonus

    for token in sentNLP:
        if token.like_num and token.lemma_ not in ques_keywords and token.lemma_ not in stopwords and not token.is_punct:
            ansDict[token.text] += ansWeights['when_matchTAG']

    if len(ansDict) == 0:
        childTokens = []  # token new in sent
        masterTokens = []  # token appear in both sent and question
        for token in sentNLP:
            if token.lemma_ not in stopwords and not token.is_punct:
                if token.lemma_ in ques_keywords:
                    masterTokens.append(token)
                else:
                    childTokens.append(token)

        for child in childTokens:
            ansDict[child.text] += ansWeights["token_exist"]
            if len(masterTokens) > 0:
                for master in masterTokens:
                    ansDict[child.text] += fabs(master.idx - child.idx) * ansWeights["token_DisMul"]
            if child.pos_ == 'NOUN':
                ansDict[child.text] += ansWeights["token_posNOUN"]

    cutoff = 1
    if len(ansDict) == 0:
        bestANS = ""
    elif len(ansDict) <= cutoff:
        bestANS = " ".join(key for key in ansDict.keys())
    else:
        sorted_keys = sorted(ansDict, key=ansDict.get, reverse=True)[:cutoff]
        bestANS = " ".join(sorted_keys)
    return bestANS


def handleWhere(questionNLP, ques_keywords, sentNLP, sentRaw):
    """

    :param questionNLP: nlp(question)
    :param ques_keywords: list of strings
    :param sentNLP: nlp(sent)
    :param sentRaw: raw sentence string
    :return: predicted best answer
    """
    ansDict = defaultdict(float)  # potential answers and their score

    for ent in sentNLP.ents:
        if ent.label_ in ['ORG', 'FACILITY', 'GPE', 'LOC']:
            ansDict[ent.text] += ansWeights['where_matchNE']
            if ent.lemma_ not in ques_keywords:
                ansDict[ent.text] += 0.1  # a bonus

    ques_kw_stemmed = [stem(kw) for kw in ques_keywords]
    sentChunks = list(sentNLP.noun_chunks)
    quesChunks = [" ".join(stemChunk(chunk)) for chunk in list(questionNLP.noun_chunks)]
    masterChunks = []  # chunks appear in both sent and question
    childChunks = []  # chunks new in sent
    for sentchunk in sentChunks:
        chunkStemmed = stemChunk(sentchunk)  # a list of string (chunk stemmed)
        if " ".join(chunkStemmed) in quesChunks:
            masterChunks.append(sentchunk)
        elif len(chunkStemmed) == 1 and chunkStemmed[0] in ques_kw_stemmed:
            masterChunks.append(sentchunk)
        else:
            childChunks.append(sentchunk)

    for child in childChunks:
        ansDict[child.text] += ansWeights['where_Chunk']
        if child.root.dep_ == 'pobj':
            ansDict[child.text] += ansWeights['where_ChunkMatchRootDep']
        if len(masterChunks) > 0:
            for master in masterChunks:
                ansDict[child.text] += (fabs(master.start_char - child.start_char) + fabs(
                    master.end_char - child.end_char)) / 2 * ansWeights["where_ChunkDisMul"]

    for token in sentNLP:
        if token.dep_ == 'pobj' and token.lemma_ not in stopwords and token.lemma_ not in ques_keywords and not token.is_punct:
            ansDict[token.text] += ansWeights['where_TokenMatchDep']

    if len(ansDict) == 0:
        childTokens = []  # token new in sent
        masterTokens = []  # token appear in both sent and question
        for token in sentNLP:
            if token.lemma_ not in stopwords and not token.is_punct:
                if token.lemma_ in ques_keywords:
                    masterTokens.append(token)
                else:
                    childTokens.append(token)

        for child in childTokens:
            ansDict[child.text] += ansWeights["token_exist"]
            if len(masterTokens) > 0:
                for master in masterTokens:
                    ansDict[child.text] += fabs(master.idx - child.idx) * ansWeights["token_DisMul"]
            if child.pos_ == 'NOUN':
                ansDict[child.text] += ansWeights["token_posNOUN"]

    cutoff = 1
    if len(ansDict) == 0:
        bestANS = ""
    elif len(ansDict) <= cutoff:
        bestANS = " ".join(key for key in ansDict.keys())
    else:
        sorted_keys = sorted(ansDict, key=ansDict.get, reverse=True)[:cutoff]
        bestANS = " ".join(sorted_keys)
    return bestANS


def handleQuantity(questionNLP, ques_keywords, sentNLP, sentRaw):
    """

    :param questionNLP: nlp(question)
    :param ques_keywords: list of strings
    :param sentNLP: nlp(sent)
    :param sentRaw: raw sentence string
    :return: predicted best answer
    """
    ansDict = defaultdict(float)  # potential answers and their score

    for ent in sentNLP.ents:
        if ent.label_ in ['QUANTITY', 'MONEY', 'PERCENT', 'ORDINAL', 'CARDINAL', 'DATE', 'TIME']:
            if '%' in ent.text or '$' in ent.text:
                ansDict[ent.lemma_] += ansWeights['quantity_matchNE']
            else:
                ansDict[ent.text] += ansWeights['quantity_matchNE']

            if ent.lemma_ not in ques_keywords:
                if '%' in ent.text or '$' in ent.text:
                    ansDict[ent.lemma_] += 0.2  # a bonus
                else:
                    ansDict[ent.text] += 0.2  # a bonus

    for token in sentNLP:
        if (token.tag_ == "CD" or token.tag_ == "PDT") and token.lemma_ not in ques_keywords and \
                        token.lemma_ not in stopwords and not token.is_punct:
            ansDict[token.text] += ansWeights['quantity_matchTAG']

    if len(ansDict) == 0:
        childTokens = []  # token new in sent
        masterTokens = []  # token appear in both sent and question
        for token in sentNLP:
            if token.lemma_ not in stopwords and not token.is_punct:
                if token.lemma_ in ques_keywords:
                    masterTokens.append(token)
                else:
                    childTokens.append(token)

        for child in childTokens:
            ansDict[child.text] += ansWeights["token_exist"]
            if len(masterTokens) > 0:
                for master in masterTokens:
                    ansDict[child.text] += fabs(master.idx - child.idx) * ansWeights["token_DisMul"]
            if child.pos_ == 'NOUN' or child.pos_ == 'ADJ':
                ansDict[child.text] += ansWeights["token_posNOUN"]

    cutoff = 1
    if len(ansDict) == 0:
        bestANS = ""
    elif len(ansDict) <= cutoff:
        bestANS = " ".join(key for key in ansDict.keys())
    else:
        sorted_keys = sorted(ansDict, key=ansDict.get, reverse=True)[:cutoff]
        bestANS = " ".join(sorted_keys)
    return bestANS


def handlePerson(questionNLP, ques_keywords, sentNLP, sentRaw):
    """

    :param questionNLP: nlp(question)
    :param ques_keywords: list of strings
    :param sentNLP: nlp(sent)
    :param sentRaw: raw sentence string
    :return: predicted best answer
    """
    ansDict = defaultdict(float)  # potential answers and their score

    for ent in sentNLP.ents:
        if ent.label_ in ["PERSON", 'NORP', 'ORG']:
            ansDict[ent.text] += ansWeights['person_matchNE']
            if ent.lemma_ not in ques_keywords:
                ansDict[ent.text] += 0.1  # a bonus

    # approximty of noun_chunk
    ques_kw_stemmed = [stem(kw) for kw in ques_keywords]
    sentChunks = list(sentNLP.noun_chunks)
    quesChunks = [" ".join(stemChunk(chunk)) for chunk in list(questionNLP.noun_chunks)]
    masterChunks = []  # chunks appear in both sent and question
    childChunks = []  # chunks new in sent
    for sentchunk in sentChunks:
        chunkStemmed = stemChunk(sentchunk)  # a list of string (chunk stemmed)
        if " ".join(chunkStemmed) in quesChunks:
            masterChunks.append(sentchunk)
        elif len(chunkStemmed) == 1 and chunkStemmed[0] in ques_kw_stemmed:
            masterChunks.append(sentchunk)
        else:
            childChunks.append(sentchunk)

    if len(childChunks) > 0:
        for child in childChunks:
            ansDict[child.text] += ansWeights["person_Chunk"]
            if len(masterChunks) > 0:
                for master in masterChunks:
                    ansDict[child.text] += (fabs(master.start_char - child.start_char) + fabs(
                        master.end_char - child.end_char)) / 2 * ansWeights["person_ChunkDisMul"]

    if len(ansDict) == 0:
        childTokens = []  # token new in sent
        masterTokens = []  # token appear in both sent and question
        for token in sentNLP:
            if token.lemma_ not in stopwords and not token.is_punct:
                if token.lemma_ in ques_keywords:
                    masterTokens.append(token)
                else:
                    childTokens.append(token)

        for child in childTokens:
            ansDict[child.text] += ansWeights["token_exist"]
            if len(masterTokens) > 0:
                for master in masterTokens:
                    ansDict[child.text] += fabs(master.idx - child.idx) * ansWeights["token_DisMul"]
            if child.pos_ == 'NOUN':
                ansDict[child.text] += ansWeights["token_posNOUN"]

    cutoff = 1
    if len(ansDict) == 0:
        bestANS = ""
    elif len(ansDict) <= cutoff:
        bestANS = " ".join(key for key in ansDict.keys())
    else:
        sorted_keys = sorted(ansDict, key=ansDict.get, reverse=True)[:cutoff]
        bestANS = " ".join(sorted_keys)
    return bestANS


def handleWhich(questionNLP, ques_keywords, sentNLP, sentRaw):
    """

    :param questionNLP: nlp(question)
    :param ques_keywords: list of strings
    :param sentNLP: nlp(sent)
    :param sentRaw: raw sentence string
    :return: predicted best answer
    """
    ansDict = defaultdict(float)  # potential answers and their score

    ques_kw_stemmed = [stem(kw) for kw in ques_keywords]
    sentChunks = list(sentNLP.noun_chunks)
    quesChunks = [" ".join(stemChunk(chunk)) for chunk in list(questionNLP.noun_chunks)]
    masterChunks = []  # chunks appear in both sent and question
    childChunks = []  # chunks new in sent
    for sentchunk in sentChunks:
        chunkStemmed = stemChunk(sentchunk)  # a list of string (chunk stemmed)
        if " ".join(chunkStemmed) in quesChunks:
            masterChunks.append(sentchunk)
        elif len(chunkStemmed) == 1 and chunkStemmed[0] in ques_kw_stemmed:
            masterChunks.append(sentchunk)
        else:
            childChunks.append(sentchunk)

    # token.dep_ and similarity
    for token in questionNLP:
        if token.text == "which":
            if token.dep_ == 'det':
                coreWord = token.head.text  # a string
                if len(childChunks) > 0:
                    for child in childChunks:
                        ansDict[child.text] += nlpSimi(child.text).similarity(nlpSimi(coreWord)) * ansWeights[
                            "which_Simi"]
                        # ansDict[child.text] += nlpSimi(child.root.text).similarity(nlpSimi(coreWord)) * ansWeights[
                        #     "which_Simi"]
            break

    # approximity of childchunks
    if len(childChunks) > 0:
        for child in childChunks:
            ansDict[child.text] += ansWeights["which_Chunk"]
            if len(masterChunks) > 0:
                for master in masterChunks:
                    ansDict[child.text] += (fabs(master.start_char - child.start_char) + fabs(
                        master.end_char - child.end_char)) / 2 * ansWeights["which_ChunkDisMul"]

    if len(ansDict) == 0:
        childTokens = []  # token new in sent
        masterTokens = []  # token appear in both sent and question
        for token in sentNLP:
            if token.lemma_ not in stopwords and not token.is_punct:
                if token.lemma_ in ques_keywords:
                    masterTokens.append(token)
                else:
                    childTokens.append(token)

        for child in childTokens:
            ansDict[child.text] += ansWeights["token_exist"]
            if len(masterTokens) > 0:
                for master in masterTokens:
                    ansDict[child.text] += fabs(master.idx - child.idx) * ansWeights["token_DisMul"]
            if child.pos_ == 'NOUN':
                ansDict[child.text] += ansWeights["token_posNOUN"]

    cutoff = 1
    if len(ansDict) == 0:
        bestANS = ""
    elif len(ansDict) <= cutoff:
        bestANS = " ".join(key for key in ansDict.keys())
    else:
        sorted_keys = sorted(ansDict, key=ansDict.get, reverse=True)[:cutoff]
        bestANS = " ".join(sorted_keys)
    return bestANS


def handleWhat(questionNLP, ques_keywords, sentNLP, sentRaw):
    """

    :param questionNLP: nlp(question)
    :param ques_keywords: list of strings
    :param sentNLP: nlp(sent)
    :param sentRaw: raw sentence string
    :return: predicted best answer
    """
    ansDict = defaultdict(float)  # potential answers and their score

    ques_kw_stemmed = [stem(kw) for kw in ques_keywords]
    sentChunks = list(sentNLP.noun_chunks)
    quesChunks = [" ".join(stemChunk(chunk)) for chunk in list(questionNLP.noun_chunks)]
    masterChunks = []  # chunks appear in both sent and question
    childChunks = []  # chunks new in sent
    for sentchunk in sentChunks:
        chunkStemmed = stemChunk(sentchunk)  # a list of string (chunk stemmed)
        if " ".join(chunkStemmed) in quesChunks:
            masterChunks.append(sentchunk)
        elif len(chunkStemmed) == 1 and chunkStemmed[0] in ques_kw_stemmed:
            masterChunks.append(sentchunk)
        else:
            childChunks.append(sentchunk)

    # token.dep_ and similarity
    for token in questionNLP:
        if token.text == "what":
            if token.dep_ == 'det':
                coreWord = token.head.text  # a string
                if len(childChunks) > 0:
                    for child in childChunks:
                        # ansDict[child.text] += nlpSimi(child.root.text).similarity(nlpSimi(coreWord)) * ansWeights[
                        #     "what_Simi"]
                        ansDict[child.text] += nlpSimi(child.text).similarity(nlpSimi(coreWord)) * ansWeights[
                            "what_Simi"]
            break

    # approximity of childchunks
    if len(childChunks) > 0:
        for child in childChunks:
            ansDict[child.text] += ansWeights["what_Chunk"]
            if len(masterChunks) > 0:
                for master in masterChunks:
                    ansDict[child.text] += (fabs(master.start_char - child.start_char) + fabs(
                        master.end_char - child.end_char)) / 2 * ansWeights[
                                               "what_ChunkDisMul"]

    if len(ansDict) == 0:
        childTokens = []  # token new in sent
        masterTokens = []  # token appear in both sent and question
        for token in sentNLP:
            if token.lemma_ not in stopwords and not token.is_punct:
                if token.lemma_ in ques_keywords:
                    masterTokens.append(token)
                else:
                    childTokens.append(token)

        for child in childTokens:
            ansDict[child.text] += ansWeights["token_exist"]
            if len(masterTokens) > 0:
                for master in masterTokens:
                    ansDict[child.text] += fabs(master.idx - child.idx) * ansWeights["token_DisMul"]
            if child.pos_ == 'NOUN':
                ansDict[child.text] += ansWeights["token_posNOUN"]

    cutoff = 1
    if len(ansDict) == 0:
        bestANS = ""
    elif len(ansDict) <= cutoff:
        bestANS = " ".join(key for key in ansDict.keys())
    else:
        sorted_keys = sorted(ansDict, key=ansDict.get, reverse=True)[:cutoff]
        bestANS = " ".join(sorted_keys)

    return bestANS


def handleWhy(questionNLP, ques_keywords, sentNLP, sentRaw):
    """

    :param questionNLP: nlp(question)
    :param ques_keywords: list of strings
    :param sentNLP: nlp(sent)
    :param sentRaw: raw sentence string
    :return: predicted best answer
    """
    ansDict = defaultdict(float)  # potential answers and their score

    ques_kw_stemmed = [stem(kw) for kw in ques_keywords]
    sentChunks = list(sentNLP.noun_chunks)
    quesChunks = [" ".join(stemChunk(chunk)) for chunk in list(questionNLP.noun_chunks)]
    masterChunks = []  # chunks appear in both sent and question
    childChunks = []  # chunks new in sent
    for sentchunk in sentChunks:
        chunkStemmed = stemChunk(sentchunk)  # a list of string (chunk stemmed)
        if " ".join(chunkStemmed) in quesChunks:
            masterChunks.append(sentchunk)
        elif len(chunkStemmed) == 1 and chunkStemmed[0] in ques_kw_stemmed:
            masterChunks.append(sentchunk)
        else:
            childChunks.append(sentchunk)

    # approximity of childchunks
    if len(childChunks) > 0:
        for child in childChunks:
            ansDict[child.text] += ansWeights["which_Chunk"]
            if len(masterChunks) > 0:
                for master in masterChunks:
                    ansDict[child.text] += (fabs(master.start_char - child.start_char) + fabs(
                        master.end_char - child.end_char)) / 2 * ansWeights[
                                               "which_ChunkDisMul"]

    if len(ansDict) == 0:
        childTokens = []  # token new in sent
        masterTokens = []  # token appear in both sent and question
        for token in sentNLP:
            if token.lemma_ not in stopwords and not token.is_punct:
                if token.lemma_ in ques_keywords:
                    masterTokens.append(token)
                else:
                    childTokens.append(token)

        for child in childTokens:
            ansDict[child.text] += ansWeights["token_exist"]
            if len(masterTokens) > 0:
                for master in masterTokens:
                    ansDict[child.text] += fabs(master.idx - child.idx) * ansWeights["token_DisMul"]
            if child.pos_ == 'NOUN':
                ansDict[child.text] += ansWeights["token_posNOUN"]

    cutoff = 1
    if len(ansDict) == 0:
        bestANS = ""
    elif len(ansDict) <= cutoff:
        bestANS = " ".join(key for key in ansDict.keys())
    else:
        sorted_keys = sorted(ansDict, key=ansDict.get, reverse=True)[:cutoff]
        bestANS = " ".join(sorted_keys)

    return bestANS


def handleDefault(questionNLP, ques_keywords, sentNLP, sentRaw):
    """

    :param questionNLP: nlp(question)
    :param ques_keywords: list of strings
    :param sentNLP: nlp(sent)
    :param sentRaw: raw sentence string
    :return: predicted best answer
    """
    ansDict = defaultdict(float)  # potential answers and their score

    ques_kw_stemmed = [stem(kw) for kw in ques_keywords]
    sentChunks = list(sentNLP.noun_chunks)
    quesChunks = [" ".join(stemChunk(chunk)) for chunk in list(questionNLP.noun_chunks)]
    masterChunks = []  # chunks appear in both sent and question
    childChunks = []  # chunks new in sent
    for sentchunk in sentChunks:
        chunkStemmed = stemChunk(sentchunk)  # a list of string (chunk stemmed)
        if " ".join(chunkStemmed) in quesChunks:
            masterChunks.append(sentchunk)
        elif len(chunkStemmed) == 1 and chunkStemmed[0] in ques_kw_stemmed:
            masterChunks.append(sentchunk)
        else:
            childChunks.append(sentchunk)

    # approximity of childchunks
    if len(childChunks) > 0:
        for child in childChunks:
            ansDict[child.text] += ansWeights["which_Chunk"]
            if len(masterChunks) > 0:
                for master in masterChunks:
                    ansDict[child.text] += (fabs(master.start_char - child.start_char) + fabs(
                        master.end_char - child.end_char)) / 2 * ansWeights[
                                               "which_ChunkDisMul"]

    if len(ansDict) == 0:
        childTokens = []  # token new in sent
        masterTokens = []  # token appear in both sent and question
        for token in sentNLP:
            if token.lemma_ not in stopwords and not token.is_punct:
                if token.lemma_ in ques_keywords:
                    masterTokens.append(token)
                else:
                    childTokens.append(token)

        for child in childTokens:
            ansDict[child.text] += ansWeights["token_exist"]
            if len(masterTokens) > 0:
                for master in masterTokens:
                    ansDict[child.text] += fabs(master.idx - child.idx) * ansWeights["token_DisMul"]
            if child.pos_ == 'NOUN':
                ansDict[child.text] += ansWeights["token_posNOUN"]

    cutoff = 1
    if len(ansDict) == 0:
        bestANS = ""
    elif len(ansDict) <= cutoff:
        bestANS = " ".join(key for key in ansDict.keys())
    else:
        sorted_keys = sorted(ansDict, key=ansDict.get, reverse=True)[:cutoff]
        bestANS = " ".join(sorted_keys)

    return bestANS


def removeStopWordInChunk(chunk):
    """
    remove stopwords, and 
    :param chunk: Spacy chunk
    :return: string
    """

    validChunk = " ".join(rmSw(Regtokenizer.tokenize(chunk.lemma_), stopwords))
    return validChunk


def rmSw(doc, stopwords):
    """
    retain 
    :param doc: list of str
    :param stopwords: 
    :return: list of str
    """
    new_doc = []
    for word in doc:
        if word in stopwords:
            continue
        new_doc.append(word)
        # new_corpus.append(new_doc)
    return new_doc


def stem(word):
    """
    stem a single word
    :param word: string
    :return: string
    """
    return stemmer.stem(word)


def stemChunk(chunk):
    imstr = removeStopWordInChunk(chunk)
    return [stem(word) for word in word_tokenize(imstr)]


""" **********************************
Begin training
********************************** """
start = time.time()

total_problem = 0
# success = 0
# sP = 2000
# span = 3000

test_pred_ans = open("QAkaggle.csv", 'w')
writer = csv.writer(test_pred_ans, delimiter=',')
initial_line = ["id", "answer"]
writer.writerow(initial_line)

pattern_whenS = re.compile(r'(what|which) (days|seasons|months|dates|times|years)')  # cutoff=2
pattern_when = re.compile(
    r'(when (do|did|will|would|does|was|is|were|are)|(what|which) (day |season|month|date|time|year))')
pattern_where = re.compile(
    r'(where (do|did|will|would|does|was|is|were|are|had|have|has)|(what|which) (place|location))')
pattern_quantity = re.compile(
    r'(how (much|many|big|small|long|often|large|far)|(which|what) (number|percentage|rank))')
pattern_person = re.compile(r'who|whom|whose')
pattern_which = re.compile(r'which')
pattern_what = re.compile(r'what')
pattern_why = re.compile(r'why')

for testing_problem in testings:  # max: 3097
    total_problem += 1
    question = testing_problem["question"].lower()  # question (string type) (in lower case)
    ques_keywords = quesKeyWords(question)  # extract question keywords (lemmatized, remove stopwords)
    doc_content = documents[testing_problem['docid']]["text"]
    bestSent = buildSents(doc_content, ques_keywords, question).lower()  # find best sentence (in lower case)
    bestSentNLP = nlp(bestSent)  # best sentence (Spacy DOC type)
    quesNLP = nlp(question)  # question sentence (Spacy DOC type)

    if pattern_whenS.search(question):
        answer = handleWhenS(quesNLP, ques_keywords, bestSentNLP, bestSent)
    elif pattern_when.search(question):
        answer = handleWhen(quesNLP, ques_keywords, bestSentNLP, bestSent)
    elif pattern_where.search(question):
        answer = handleWhere(quesNLP, ques_keywords, bestSentNLP, bestSent)
    elif pattern_quantity.search(question):
        answer = handleQuantity(quesNLP, ques_keywords, bestSentNLP, bestSent)
    elif pattern_what.search(question):
        answer = handleWhat(quesNLP, ques_keywords, bestSentNLP, bestSent)
    elif pattern_which.search(question):
        answer = handleWhich(quesNLP, ques_keywords, bestSentNLP, bestSent)
    elif pattern_person.search(question):
        answer = handlePerson(quesNLP, ques_keywords, bestSentNLP, bestSent)
    elif pattern_why.search(question):
        answer = handleWhy(quesNLP, ques_keywords, bestSentNLP, bestSent)
    else:
        answer = handleDefault(quesNLP, ques_keywords, bestSentNLP, bestSent)

    new_row = [testing_problem['id'], answer]
    writer.writerow(new_row)

    if total_problem % 10 == 0:  # measure speed of program
        print('done', total_problem)

test_pred_ans.close()
end = time.time()
print("Time usage [min]:", (end - start) / 60)
