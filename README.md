# Question Answering System

### Installation Requirement
In order to smoothly run the python code, the following external packages needs to be installed:

* **nltk**
* **spacy**

Especially, for **spacy**, two language models needs to be installed:

* `en_core_web_md`
* `en_vectors_web_lg`

You can download these two models using the following command lines:

* `python3 -m spacy download en_core_web_md`
* `python3 -m spacy download en_vectors_web_lg`

### Usage Instruction

To run the python code ***qaSystem.py***, type this line in terminal:
`	python3 qaSystem.py`

This python code will output a file ***QAkaggle.csv***, which is used in Kaggle competition.


### Report
For a brief introduction of the project, and also the outcome/analysis of the project, please refer to the file ***report.pdf***